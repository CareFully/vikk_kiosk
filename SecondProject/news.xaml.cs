﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Syndication;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SecondProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class News : Page
    {
        public News()
        {
            this.InitializeComponent();
            getNews();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            NavigationRootPage.Current.Title = "Uudised";
        }

        public async void getNews()
        {
            
            try
            {
                SyndicationClient client = new SyndicationClient();
                SyndicationFeed feed;
                Uri uri = null;
                string uriString = "https://vikk.ee/?format=feed&type=rss";
                uri = new Uri(uriString);

                //client.SetRequestHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
                feed = await client.RetrieveFeedAsync(uri);
                foreach (Windows.Web.Syndication.SyndicationItem item in feed.Items)
                {
                    string itemTitle = item.Title == null ? "No title" : item.Title.Text;
                    string itemSummary = item.Summary == null ? "No content" : item.Summary.Text;
                    /* Nvm
                    var wolksvagen= new WebView();
                    wolksvagen.Width = 880;
                    wolksvagen.Height = 100;
                    wolksvagen.NavigateToString("<h1>" + itemTitle + "</h1><br/>" + item.Summary.Text);
                    this.rssfeedlist1.Items.Add(wolksvagen);
                    */
                    var meow = new TextBlock();
                    meow.Text = itemTitle;// + "\n" + itemSummary + "\n";
                    meow.Tag = item;
                    

                    this.rssfeedlist1.Items.Add(meow);
                }
                progressRing.IsActive = false;
                progressRing.Visibility = Visibility.Collapsed;

            }
            catch (Exception ex)
            {
                
            }
        }

        private void rssfeedlist1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tb=(TextBlock)rssfeedlist1.SelectedItem;
            var item = (SyndicationItem)tb.Tag;
            try
            {
                stackpanel1.Children.Clear();
                tb = new TextBlock();
                tb.TextWrapping = new TextWrapping();
                tb.Text = item.Title.Text;
                tb.FontSize = 24;

                tb.Padding=new Thickness(4);
                stackpanel1.Children.Add(tb);

                tb = new TextBlock();
                tb.TextWrapping=new TextWrapping();
                tb.Text = Regex.Replace(Regex.Replace(item.Summary.Text, @"<[^>]+>|&nbsp;", "").Trim(), @"\s{2,}", " ");
                stackpanel1.Children.Add(tb);
            }
            catch(Exception){

            }
            
        }
    }
}
