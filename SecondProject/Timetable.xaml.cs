﻿using Newtonsoft.Json;
using SecondProject.GroupsListJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SecondProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Timetable : Page
    {
        private Group group;

        public Timetable()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.Parameter is Group)
            {
                group = (Group)e.Parameter;
                NavigationRootPage.Current.Title = group.nimi;
                loadTimetable(group);
            }
        }

        private async void loadTimetable(Group group)
        {
            // Get week's monday
            DateTime monday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
            string date = monday.ToString("yyyy-MM-dd");

            //Create an HTTP client object
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            String url = String.Format("https://vikk.siseveeb.ee/veebilehe_andmed/tunniplaan/?nadal={0}&{1}={2}", date, group.type, group.id);
            Uri requestUri = new Uri(url);
            //debugText.Text = requestUri.AbsoluteUri;
            // Send the GET request asynchronously and retrieve the response as a string.
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = "";

            try
            {
                //Send the GET request
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                TimetableListJson.Timetable data = JsonConvert.DeserializeObject<TimetableListJson.Timetable>(httpResponseBody); ;

                ListDay1.ItemsSource = data.tunnid.esmaspaev;
                ListDay2.ItemsSource = data.tunnid.teisipaev;
                ListDay3.ItemsSource = data.tunnid.kolmapaev;
                ListDay4.ItemsSource = data.tunnid.neljapaev;
                ListDay5.ItemsSource = data.tunnid.reede;
            }
            catch (Exception ex)
            {
                //debugText.Text = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
                Console.WriteLine("Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message);
            }
            progressRing.IsActive = false;
            progressRing.Visibility = Visibility.Collapsed;
        }
    }
}
