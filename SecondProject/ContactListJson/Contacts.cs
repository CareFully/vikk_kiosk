﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject.ContactListJson
{
    /*
    public class Contacts
    {
        public string Type { get; set; }
        public List<Contact> ContactsList { get; set; }
    }
    */

    public class Employee
    {
        public string job_title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public List<string> room { get; set; }
        public List<string> phone { get; set; }
        public List<string> cell_phone { get; set; }

        public string name
        {
            get
            {
                return firstname + " " + lastname;
            }
        }

        public string phones
        {
            get
            {
                string all_phones = "";
                string phones = "";
                string cell_phones = "";
                IEnumerable<string> united_phones;
               
                // If phones exist
                if (phone != null && phone.Count() > 0)
                {
                    // If cell_phones exist also
                    if(cell_phone != null && cell_phone.Count() > 0)
                    {
                        united_phones = phone.Union(cell_phone);
                        return all_phones = String.Join(", ", united_phones);
                    }
                    else
                    {
                        return phones = String.Join(", ", phone);
                    }

                }
                // If cell_phones only exist
                else if(cell_phone != null && cell_phone.Count() > 0)
                {
                   return cell_phones = String.Join(", ", cell_phone);
                }
                return "";
            }
        }

        public string rooms
        {
            get
            {
                if(room != null)
                {
                    return String.Join(", ", room.ToArray());
                }
                return "";
            }
        }

        public Employee(string job_title, string firstname, string lastname, string email, List<string> room, List<string> phone, List<string> cell_phone)
        {
            this.job_title = job_title;
            this.firstname = firstname;
            this.lastname = lastname;
            this.email = email;
            this.room = room;
            this.phone = phone;
            this.cell_phone = cell_phone;
        }
    }

    public class Contacts
    {
        public string section_name { get; set; }
        public List<Employee> employees { get; set; }
    }
}
