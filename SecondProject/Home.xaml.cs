﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SecondProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Home : Page
    {
        public Home()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            NavigationRootPage.Current.Title = "Avaleht";
            // Add sample data
          
            List<NavigationItem> navItems = new List<NavigationItem>()
            {
                new NavigationItem("Gruppide tunniplaan", typeof(Groups), "grupp"),
                new NavigationItem("Õpetajate tunniplaan", typeof(Groups), "opetaja"),
                new NavigationItem("Ruumide tunniplaan", typeof(Groups), "ruum"),
                new NavigationItem("Vabad ruumid", typeof(FreeRooms)),
                new NavigationItem("Kontaktid", typeof(ContactsGroups)),
                new NavigationItem("Uudised", typeof(News))
            };
            navigationList.ItemsSource = navItems;

            List<Notification> notifications = new List<Notification>();
            for (int i = 0; i < 5; i++)
            {
                notifications.Add(new Notification("Üleriietes tunnis olla ei tohi",
                    "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
                    "Tarmo Loodus",
                    "24.11.2017"));
            }

            notificationsList.ItemsSource = notifications;

        }

        private void mainList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var item = (NavigationItem) e.ClickedItem;
            NavigationRootPage.RootFrame.Navigate(item.Page, item.Tag);
        }
    }
}














