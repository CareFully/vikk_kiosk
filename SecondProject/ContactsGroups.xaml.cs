﻿using Newtonsoft.Json;
using SecondProject.SelectionsListJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SecondProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ContactsGroups : Page
    {
        public ContactsGroups()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            NavigationRootPage.Current.Title = "Kontaktid";
            loadDataAsync();

        }

        private async void loadDataAsync()
        {
            //Create an HTTP client object
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri("https://vikk.siseveeb.ee/veebilehe_andmed/tootajad");
            // Send the GET request asynchronously and retrieve the response as a string.
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = "";

            try
            {
                //Send the GET request
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                Employees data = JsonConvert.DeserializeObject<Employees>(httpResponseBody);
                List<BaseSection> listItems = new List<BaseSection>();
                foreach (KeyValuePair<string, string> entry in data.sections)
                {
                    listItems.Add(new BaseSection(entry.Key, entry.Value));
                }

                contactGroupsList.ItemsSource = listItems;
            }
            catch (Exception ex)
            {
                //debugText.Text = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
                Console.WriteLine("Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message);
            }
            progressRing.IsActive = false;
            progressRing.Visibility = Visibility.Collapsed;


        }

        private void contactGroupsList_ItemClick(object sender, ItemClickEventArgs e)
        {
            BaseSection section = (BaseSection)e.ClickedItem;
            NavigationRootPage.RootFrame.Navigate(typeof(Contacts), section);
        }
    }
}
