﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject.RoomsListJson
{
    class FreeRoom
    {
        public int hoone_id { get; set; }
        public int ruumi_id { get; set; }
        public string hoone_nimi { get; set; }
        public string ruumi_nr { get; set; }
        public string ruumi_nimi { get; set; }
    }
}
