﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject.GroupsListJson
{
    public class Groups : BaseGroups
    {
        public List<Group> grupp { get; set; }

        public override List<Group> GetGroups()
        {
            return grupp;
        }
    }
}
