﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject.GroupsListJson
{
    public class Teachers : BaseGroups
    {
        public List<Group> opetaja { get; set; }

        public override List<Group> GetGroups()
        {
            return opetaja;
        }
    }
}
