﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject.GroupsListJson
{
    public class Rooms : BaseGroups
    {
        public List<Group> ruum { get; set; }

        public override List<Group> GetGroups()
        {
            return ruum;
        }
    }
}
