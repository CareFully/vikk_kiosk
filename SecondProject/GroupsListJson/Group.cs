﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject.GroupsListJson
{
    public class Group
    {
        public string id { get; set; }
        public string nimi { get; set; }
        public string type { get; set; }
    }
}
