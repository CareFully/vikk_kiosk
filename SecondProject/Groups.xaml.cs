﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SecondProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Groups : Page
    {
        private static readonly Dictionary<string, string> _titles
            = new Dictionary<string, string>()
            {
                { "grupp", "Gruppide tunniplaan" },
                { "opetaja", "Õpetajate tunniplaan" },
                { "ruum", "Ruumide tunniplaan" }
            };

        string type;
        public Groups()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.Parameter is string)
            {
                var item = (string) e.Parameter;
                type = item;
            }
            else
            {
                type = "grupp";
            }
            NavigationRootPage.Current.Title = _titles.GetValueOrDefault(type, "Gruppide tunniplaan");
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationRootPage.Current.SetSelectedNavigationItem(type);
            loadDataAsync();
        }

        private async void loadDataAsync()
        {
            // Get week's monday
            DateTime monday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
            string date = monday.ToString("yyyy-MM-dd");

            //Create an HTTP client object
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri("https://vikk.siseveeb.ee/veebilehe_andmed/tunniplaan/?nadal=" + date + "&nimekiri=" + type);
            //debugText.Text = requestUri.AbsoluteUri;
            // Send the GET request asynchronously and retrieve the response as a string.
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = "";

            try
            {
                //Send the GET request
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                GroupsListJson.BaseGroups data;

                switch(type)
                {
                    case "opetaja":
                        data = JsonConvert.DeserializeObject<GroupsListJson.Teachers>(httpResponseBody);
                        break;
                    case "ruum":
                        data = JsonConvert.DeserializeObject<GroupsListJson.Rooms>(httpResponseBody);
                        break;
                    case "grupp":
                    default:
                        data = JsonConvert.DeserializeObject<GroupsListJson.Groups>(httpResponseBody);
                        break;
                }

                List<GroupsListJson.Group> listItems = data.GetGroups();

                mainList.ItemsSource = listItems;
            }
            catch (Exception ex)
            {
                debugText.Text = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
            progressRing.IsActive = false;
            progressRing.Visibility = Visibility.Collapsed;


        }

        private void mainList_ItemClick(object sender, ItemClickEventArgs e)
        {
            var item = (GroupsListJson.Group) e.ClickedItem;
            item.type = type;
            NavigationRootPage.RootFrame.Navigate(typeof(Timetable), item);
            debugText.Text = item.nimi;
        }
    }
}
