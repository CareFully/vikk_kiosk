﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SecondProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NavigationRootPage : Page
    {
        public static NavigationRootPage Current;
        public static Frame RootFrame = null;

        private RootFrameNavigationHelper navigationHelper;

        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(NavigationRootPage), new PropertyMetadata(null));

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public NavigationView NavigationView
        {
            get { return navView; }
        }


        public NavigationRootPage()
        {
            this.InitializeComponent();
            this.DataContext = this;
            Current = this;
            RootFrame = contentFrame;
            navigationHelper = new RootFrameNavigationHelper(contentFrame);

        }

        private void navView_Loaded(object sender, RoutedEventArgs e)
        {
            RootFrame.Navigate(typeof(Home));
        }

        private void navView_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {

        }

        public void SetSelectedNavigationItem(string Tag)
        {
            var menuItem = Current.NavigationView.MenuItems.Cast<NavigationViewItemBase>().FirstOrDefault(m => m?.Tag.ToString() == Tag);
            if (menuItem != null)
            {
                menuItem.IsSelected = true;
            }
        }

        private void ResetBackStack()
        {
            navigationHelper.ClearBackStack();
        }

        private void navView_SelectionChanged(NavigationView sender, NavigationViewSelectionChangedEventArgs args)
        {

            if(args.SelectedItem is NavigationViewItem)
            {
                NavigationViewItem item = args.SelectedItem as NavigationViewItem;

                switch (item.Tag)
                {
                    case "home":
                        contentFrame.Navigate(typeof(Home));
                        ResetBackStack();
                        break;
                    case "grupp":
                        contentFrame.Navigate(typeof(Groups), item.Tag);
                        break;
                    case "opetaja":
                        contentFrame.Navigate(typeof(Groups), item.Tag);
                        break;
                    case "ruum":
                        contentFrame.Navigate(typeof(Groups), item.Tag);
                        break;
                    case "free_rooms":
                        contentFrame.Navigate(typeof(FreeRooms));
                        break;
                    case "contacts":
                        contentFrame.Navigate(typeof(ContactsGroups));
                        break;
                    case "news":
                        contentFrame.Navigate(typeof(News));
                        break;
                    case "current_lessons":
                        contentFrame.Navigate(typeof(CurrentLessons));
                        break;
                }
            }
        }
    }
}
