﻿using Newtonsoft.Json;
using SecondProject.RoomsListJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SecondProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FreeRoomsList : Page
    {
        public FreeRoomsList()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.Parameter is string)
            {
                string scope = (string)e.Parameter;
                loadRooms(scope);
            }
        }
        private async void loadRooms(string scope)
        {
            //Create an HTTP client object
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            String url = String.Format("https://vikk.siseveeb.ee/veebilehe_andmed/vabad_ruumid?skoop={0}", scope);
            Uri requestUri = new Uri(url);

            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = "";

            try
            {
                //Send the GET request
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                List<FreeRoom> data = JsonConvert.DeserializeObject<List<FreeRoom>>(httpResponseBody); ;

                RoomsList.ItemsSource = data;
            }
            catch (Exception ex)
            {
                //debugText.Text = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
                Console.WriteLine("Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message);
            }
            progressRing.IsActive = false;
            progressRing.Visibility = Visibility.Collapsed;
        }

    }
}
