﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject.SelectionsListJson
{
    class BaseSection
    {
        public string section { get; set; }
        public string name { get; set; }

        public BaseSection(string section, string name)
        {
            this.section = section;
            this.name = name;
        }
    }
}
