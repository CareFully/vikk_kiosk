﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject.TimetableListJson
{
    public class Tunnid
    {
        public List<Lesson> esmaspaev { get; set; }
        public List<Lesson> teisipaev { get; set; }
        public List<Lesson> kolmapaev { get; set; }
        public List<Lesson> neljapaev { get; set; }
        public List<Lesson> reede { get; set; }
    }
}
