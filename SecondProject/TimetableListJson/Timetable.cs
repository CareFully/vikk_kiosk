﻿
namespace SecondProject.TimetableListJson
{
    public class Timetable
    {
        public string nadal { get; set; }
        public Tunnid tunnid { get; set; }
        public string viimane_muudatus { get; set; }
    }
}
