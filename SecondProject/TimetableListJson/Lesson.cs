﻿
using System;

namespace SecondProject.TimetableListJson
{
    public class Lesson
    {
        public string tund { get; set; }
        private string _aine;
        public string aine {
            get
            {
                return _aine;
            }
            set {
                if(value.Length > 0)
                {
                    // Uppercase first letter
                    _aine = char.ToUpper(value[0]) + value.Substring(1).ToLower();
                } else
                {
                    _aine = value;
                }
            }
        }
        public string opetaja { get; set; }
        public string algus { get; set; }
        public string lopp { get; set; }
        public string ruum { get; set; }
        public string grupp { get; set; }

        public string time
        {
            get
            {
                return String.Format("{0} - {1}", algus, lopp);
            }
        }
    }
}
