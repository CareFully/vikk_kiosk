﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject
{
    public class NavigationItem
    {
        public string Title { get; set; }
        public Type Page { get; set; }
        public object Tag { get; set; }
        public NavigationItem(string title, Type page)
        {
            Title = title;
            Page = page;
            Tag = null;
        }

        public NavigationItem(string title, Type page, object tag)
        {
            Title = title;
            Page = page;
            Tag = tag;
        }
    }
}
