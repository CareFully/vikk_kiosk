﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondProject
{
    public class Notification
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Writer { get; set; }
        public string Date { get; set; }

        public string Footer
        {
            get { return Writer + ", " + Date; }
        }

        public Notification(string title, string content, string writer, string date)
        {
            Title = title;
            Content = content;
            Writer = writer;
            Date = date;
        }
    }
}
